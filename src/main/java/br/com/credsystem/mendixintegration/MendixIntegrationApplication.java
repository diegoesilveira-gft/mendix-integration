package br.com.credsystem.mendixintegration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MendixIntegrationApplication {

	public static void main(String[] args) {
		SpringApplication.run(MendixIntegrationApplication.class, args);
	}

}
