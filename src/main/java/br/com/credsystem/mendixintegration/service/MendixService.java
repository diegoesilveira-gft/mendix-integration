package br.com.credsystem.mendixintegration.service;

import br.com.credsystem.mendixintegration.controller.CreateStoryEndpoint;
import br.com.credsystem.mendixintegration.controller.UpdateStoryEndpoint;
import br.com.credsystem.mendixintegration.integration.client.CreateStoryClient;
import br.com.credsystem.mendixintegration.integration.client.MendixClient;
import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStory;
import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStoryResponse;
import br.com.credsystem.mendixintegration.integration.soap_generate.ObjectFactory;
import br.com.credsystem.mendixintegration.model.request.CreateStoryRequest;
import br.com.credsystem.mendixintegration.model.request.UpdateStoryRequest;
import br.com.credsystem.mendixintegration.model.response.UpdateStoryResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.SoapMessage;

import javax.xml.bind.JAXBElement;
import javax.xml.soap.SOAPException;
import java.util.Objects;

@Service
public class MendixService {

    @Autowired
    private transient MendixClient mendixClient;

    @Autowired
    private transient UpdateStoryEndpoint endpoint;

    @Autowired
    private transient CreateStoryEndpoint createStoryEndpoint;


    public CreateStoryResponse createStory(CreateStoryRequest createStoryRequest, String apiKey){
       // CreateStoryResponse createStoryResponse = CreateStoryResponse.builder().value(1L).build();
       // if (Objects.isNull(apiKey))
       //     throw new RuntimeException("Erro apiKey em branco!");
       // return createStoryResponse;
        CreateStoryClient createStory = createStoryEndpoint.getCreateStoryRequest(createStoryRequest, apiKey);
        if (Objects.nonNull(createStory)) {
            ObjectFactory objectFactory = new ObjectFactory();
            CreateStoryResponse createStoryResponse = objectFactory.createCreateStoryResponse();
            createStoryResponse.setNewStoryID(1L);
            return createStoryResponse;
        }
        return null;
    }

    public UpdateStoryResponse updateStory(UpdateStoryRequest request, String apiKey){
        //UpdateStoryResponse updateStoryResponse = mendixClient.getUpdateStoryResponse(request, apiKey);
        UpdateStoryResponse updateStoryResponse = endpoint.getUpdateStoryResponse(request);
        return updateStoryResponse;
    }


}
