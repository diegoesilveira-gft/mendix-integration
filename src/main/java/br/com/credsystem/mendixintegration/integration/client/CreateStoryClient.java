package br.com.credsystem.mendixintegration.integration.client;

import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStory;
import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStoryResponse;
import br.com.credsystem.mendixintegration.integration.soap_generate.ObjectFactory;
import br.com.credsystem.mendixintegration.model.request.CreateStoryRequest;
import com.sun.xml.messaging.saaj.soap.SOAPDocumentImpl;
import com.sun.xml.messaging.saaj.soap.SOAPPartImpl;
import com.sun.xml.messaging.saaj.soap.ver1_1.Body1_1Impl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.w3c.dom.NodeList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;
import javax.xml.soap.*;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Iterator;


public class CreateStoryClient implements WebServiceMessageCallback {


    @Autowired
    private CreateStoryRequest request;
    private String apiKey;
    private SOAPEnvelope envelope;

    public CreateStoryClient(CreateStoryRequest request, String apiKey) {
        this.request = request;
        this.apiKey = apiKey;
        String soapEndpointUrl = "http://home.mendix.com/stories";
        String soapAction = "http://home.mendix.com/storiesCreateStory";

        callSoapWebService(soapEndpointUrl, soapAction);
    }


    private void createSoapEnvelope(SOAPMessage soapMessage) throws SOAPException {

        // SOAP Envelope
        envelope = soapMessage.getSOAPPart().getEnvelope();
            /*
            Constructed SOAP Request Message:
            <SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/" xmlns:myNamespace="https://www.w3schools.com/xml/">
                <SOAP-ENV:Header/>
                <SOAP-ENV:Body>
                    <myNamespace:CelsiusToFahrenheit>
                        <myNamespace:Celsius>100</myNamespace:Celsius>
                    </myNamespace:CelsiusToFahrenheit>
                </SOAP-ENV:Body>
            </SOAP-ENV:Envelope>
            */

        // SOAP Body

        SOAPBody body = envelope.getBody();
        CreateStory element = criarStoryToMendix(request, apiKey);

        SOAPElement soapBodyElement= body.addChildElement("CreateStory");
        soapBodyElement.isDefaultNamespace("http://home.mendix.com/stories");
        soapBodyElement.addAttribute(QName.valueOf("name"), element.getName());
        soapBodyElement.addAttribute(QName.valueOf("description"), element.getDescription());
        soapBodyElement.addAttribute(QName.valueOf("storyType"), element.getStoryType());
        soapBodyElement.addAttribute(QName.valueOf("points"), element.getPoints().getValue());
        soapBodyElement.addAttribute(QName.valueOf("projectID"), element.getProjectID());
        soapBodyElement.addAttribute(QName.valueOf("apiKey"), apiKey);
        soapBodyElement.addAttribute(QName.valueOf("sprintID"), String.valueOf(element.getSprintID()));

       body.addChildElement(soapBodyElement.getElementName());

   }

    private void callSoapWebService(String soapEndpointUrl, String soapAction) {
        try {
            // Create SOAP Connection
            SOAPConnectionFactory soapConnectionFactory = SOAPConnectionFactory.newInstance();
            SOAPConnection soapConnection = soapConnectionFactory.createConnection();

            // Send SOAP Message to SOAP Server
            SOAPMessage message = createSOAPRequest(soapAction, soapConnection, soapEndpointUrl);
           // SOAPMessage soapResponse = soapConnection.call(message, soapEndpointUrl);

            // Print the SOAP Response
            System.out.println("Response SOAP Message:");
            message.writeTo(System.out);
            System.out.println();

            soapConnection.close();
        } catch (Exception e) {
            System.err.println("\nError occurred while sending SOAP Request to Server!\nMake sure you have the correct endpoint URL and SOAPAction!\n");
            e.printStackTrace();
        }
    }

    private SOAPMessage createSOAPRequest(String soapAction, SOAPConnection soapConnection, String soapEndpointUrl) throws Exception {
        MessageFactory messageFactory = MessageFactory.newInstance(SOAPConstants.SOAP_1_2_PROTOCOL);
        SOAPMessage soapMessage = messageFactory.createMessage();

        MimeHeaders headers = soapMessage.getMimeHeaders();
        headers.addHeader("SOAPAction", soapAction);
        headers.addHeader("username" , "carlos.arlindo@gft.com");
        headers.addHeader("password", apiKey);
        createSoapEnvelope(soapMessage);
        soapMessage.saveChanges();
        /* Print the request message, just for debugging purposes */
        System.out.println("Request SOAP Message:");
        soapMessage.writeTo(System.out);
        System.out.println("\n");
        SOAPMessage soapResponse = soapConnection.call(soapMessage, soapEndpointUrl);
        return soapMessage;
    }



    private CreateStory criarStoryToMendix(CreateStoryRequest request, String apiKey) {
        ObjectFactory objectFactory = new ObjectFactory();
        CreateStory createStory = objectFactory.createCreateStory();

        JAXBElement points = objectFactory.createCreateStoryPoints(request.getPoints());

        createStory.setApiKey(apiKey);
        createStory.setStoryType(request.getStoryType());
        createStory.setDescription(request.getDescription());
        createStory.setName(request.getName());
        createStory.setPoints(points);
        createStory.setProjectID(request.getProjectId());

        return createStory;
    }

    @Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
        String soapEndpointUrl = "http://schemas.xmlsoap.org/soap/http/CreateStory.asmx";
        String soapAction = "http://home.mendix.com/storiesCreateStory";

        callSoapWebService(soapEndpointUrl, soapAction);
    }
}
