package br.com.credsystem.mendixintegration.integration.config;

import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.PathResource;
import org.springframework.core.io.Resource;
import org.springframework.ws.config.annotation.EnableWs;
import org.springframework.ws.config.annotation.WsConfigurerAdapter;
import org.springframework.ws.transport.http.MessageDispatcherServlet;
import org.springframework.ws.wsdl.wsdl11.DefaultWsdl11Definition;
import org.springframework.xml.xsd.SimpleXsdSchema;
import org.springframework.xml.xsd.XsdSchema;

import java.io.File;
import java.io.IOException;

@EnableWs
@Configuration
public class WebServiceConfig extends WsConfigurerAdapter {
        @Bean
        public ServletRegistrationBean<MessageDispatcherServlet> messageDispatcherServlet(ApplicationContext applicationContext) {
            MessageDispatcherServlet servlet = new MessageDispatcherServlet();
            servlet.setApplicationContext(applicationContext);
            servlet.setTransformWsdlLocations(true);
            return new ServletRegistrationBean<>(servlet, "/ws/*");
        }

    @Bean(name = "createStoryWsdl")
    public DefaultWsdl11Definition defaultWsdl11Definition(XsdSchema createStorySchema)
    {
        DefaultWsdl11Definition wsdl11Definition = new DefaultWsdl11Definition();
        wsdl11Definition.setPortTypeName("StoriesAPIPortType");
        wsdl11Definition.setLocationUri("/ws/createStory");
        wsdl11Definition.setTargetNamespace("http://home.mendix.com/stories");
        wsdl11Definition.setSchema(createStorySchema);
        return wsdl11Definition;
    }

    @Bean
    public XsdSchema createStorySchema() throws IOException {
        //new ClassPathResource("createStory.xsd")
          return new SimpleXsdSchema(new PathResource("src/main/resources/xsd/createStory.xsd"));
    }
}
