package br.com.credsystem.mendixintegration.integration.config;

import br.com.credsystem.mendixintegration.integration.client.MendixClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.ws.config.annotation.EnableWs;

@Configuration
//@EnableWs
public class SoapConfig {

    @Bean
    public Jaxb2Marshaller marshaller(){
        Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
        marshaller.setContextPath("br.com.credsystem.mendixintegration.integration.soap_generate");
        return marshaller;
    }

    @Bean
    public MendixClient mendixClient(Jaxb2Marshaller marshaller){
        MendixClient mendixClient = new MendixClient();
        mendixClient.setDefaultUri("https://sprintr.home.mendix.com/ws/StoriesAPI");
        mendixClient.setMarshaller(marshaller);
        mendixClient.setUnmarshaller(marshaller);
        return mendixClient;
    }
}
