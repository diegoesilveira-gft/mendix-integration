package br.com.credsystem.mendixintegration.integration.client;

import lombok.extern.slf4j.Slf4j;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.soap.SoapHeader;
import org.springframework.ws.soap.SoapMessage;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.IOException;

@Slf4j
public class SecurityHeader implements WebServiceMessageCallback {

    private AuthenticationClient authentication;

    public SecurityHeader(AuthenticationClient authentication) {
        this.authentication = authentication;
    }

    @Override
    public void doWithMessage(WebServiceMessage webServiceMessage) throws IOException {
        SoapHeader soapHeader = ((SoapMessage)webServiceMessage).getSoapHeader();


        try{
            JAXBContext context = JAXBContext.newInstance(AuthenticationClient.class);

            Marshaller marshaller = context.createMarshaller();
            marshaller.marshal(authentication, soapHeader.getResult());

        } catch (JAXBException e) {
            throw new IOException("Error while marshalling authentication.");
        }
    }
}
