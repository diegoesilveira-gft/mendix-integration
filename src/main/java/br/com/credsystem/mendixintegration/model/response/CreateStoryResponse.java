package br.com.credsystem.mendixintegration.model.response;

import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStory;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CreateStoryResponse {

    //private Long value;
    private CreateStory story;
}
