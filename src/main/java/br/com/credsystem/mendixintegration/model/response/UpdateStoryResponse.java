package br.com.credsystem.mendixintegration.model.response;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UpdateStoryResponse {

    private Boolean value;
}
