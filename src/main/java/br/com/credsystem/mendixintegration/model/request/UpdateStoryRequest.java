package br.com.credsystem.mendixintegration.model.request;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UpdateStoryRequest {

    @JsonProperty(value = "storyId")
    private Long storyId;
    @JsonProperty(value = "name")
    private String name;
    @JsonProperty(value = "description")
    private String description;
    @JsonProperty(value = "status")
    private String status;
    @JsonProperty(value = "storyType")
    private String storyType;
    @JsonProperty(value = "points")
    private String points;
    @JsonProperty(value = "parentSprintId")
    private Long parentSprintId;
    @JsonProperty(value = "projectId")
    private String projectId;
}
