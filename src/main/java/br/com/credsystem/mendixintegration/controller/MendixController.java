package br.com.credsystem.mendixintegration.controller;

import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStoryResponse;
import br.com.credsystem.mendixintegration.model.request.CreateStoryRequest;
import br.com.credsystem.mendixintegration.model.request.UpdateStoryRequest;
import br.com.credsystem.mendixintegration.model.response.UpdateStoryResponse;
import br.com.credsystem.mendixintegration.service.MendixService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(path = "/v1/mendix")
public class MendixController {

    @Autowired
    private transient MendixService service;

    public MendixController(MendixService service) {
        this.service = service;
    }

    @PostMapping(value = "/create-story")
    @ApiOperation(value = "createStoryInTeamServer", httpMethod = "POST", produces = "application/json")
    public CreateStoryResponse createStory(@RequestBody CreateStoryRequest createStoryRequest, @RequestHeader("Mendix-ApiKey") String apiKey){
        return service.createStory(createStoryRequest, apiKey);
    }

    @PutMapping(value = "/update-story")
    @ApiOperation(value = "updateStoryInTeamServer", httpMethod = "PUT", produces = "application/json")
    public UpdateStoryResponse updateStory(@RequestBody UpdateStoryRequest request, @RequestHeader("apikey") String apikey){
        return service.updateStory(request, apikey);
    }
}
