package br.com.credsystem.mendixintegration.controller;


import br.com.credsystem.mendixintegration.model.request.UpdateStoryRequest;
import br.com.credsystem.mendixintegration.model.response.UpdateStoryResponse;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapBody;
import org.springframework.ws.soap.SoapEnvelope;
import org.springframework.ws.soap.SoapHeader;

import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.util.Objects;

@Endpoint
public class UpdateStoryEndpoint implements WebServiceMessageCallback {

    private static final String NAMESPACE_URI = "https://sprintr.home.mendix.com/ws/StoriesAPI";


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "UpdateStory")
    @ResponsePayload
    public UpdateStoryResponse getUpdateStoryResponse(@RequestPayload UpdateStoryRequest updateStoryRequest) {

        /*UpdateStoryResponse updateStoryResponse = new UpdateStoryResponse();
        if (Objects.nonNull(updateStoryRequest)) {

            updateStoryResponse.setValue(true);
        } else {
            updateStoryResponse.setValue(false);
        }
        return updateStoryResponse; */
        return null;
    }

    @Override
    public void doWithMessage(WebServiceMessage message) throws IOException, TransformerException {
        SoapEnvelope soapEnvelope = ((SoapEnvelope) message);
        SoapBody soapBody = soapEnvelope.getBody();
        SoapHeader soapHeader = soapEnvelope.getHeader();

    }


}
