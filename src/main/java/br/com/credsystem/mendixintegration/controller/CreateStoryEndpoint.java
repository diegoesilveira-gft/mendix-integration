package br.com.credsystem.mendixintegration.controller;

import br.com.credsystem.mendixintegration.integration.client.CreateStoryClient;
import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStory;
import br.com.credsystem.mendixintegration.integration.soap_generate.CreateStoryResponse;
import br.com.credsystem.mendixintegration.integration.soap_generate.ObjectFactory;
import br.com.credsystem.mendixintegration.model.request.CreateStoryRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceMessageCallback;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.SoapMessage;

import javax.xml.bind.JAXBElement;
import javax.xml.transform.TransformerException;
import java.io.IOException;

@Endpoint
public class CreateStoryEndpoint {

    private static final String NAMESPACE_URI = "https://sprintr.home.mendix.com/ws/StoriesAPI";

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "CreateStory")
    @ResponsePayload
    public CreateStoryClient getCreateStoryRequest(@RequestPayload CreateStoryRequest request, String apiKey) {
        CreateStoryClient client = new CreateStoryClient(request, apiKey);
        return client;

    }

    public CreateStoryResponse getCreateStoryResponse() {
        ObjectFactory objectFactory = new ObjectFactory();
        CreateStoryResponse response = objectFactory.createCreateStoryResponse();
        response.setNewStoryID(1L);

        return response;
    }
}
